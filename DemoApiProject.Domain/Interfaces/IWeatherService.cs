﻿namespace DemoApiProject.Domain.Interfaces
{
    public interface IWeatherService
    {
        int GetTemperature();
    }
}
