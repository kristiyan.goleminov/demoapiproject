﻿using System;
using DemoApiProject.Domain.Interfaces;

namespace DemoApiProject.Domain.Services
{
    public class WeatherService : IWeatherService
    {
        public int GetTemperature()
        {
            var rng = new Random();

            return rng.Next(-20, 55);
        }
    }
}
