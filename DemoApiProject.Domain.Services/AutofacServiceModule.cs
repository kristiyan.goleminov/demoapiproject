﻿using Autofac;
using DemoApiProject.Domain.Interfaces;

namespace DemoApiProject.Domain.Services
{
    public class AutofacServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<WeatherService>().As<IWeatherService>();
        }
    }
}
